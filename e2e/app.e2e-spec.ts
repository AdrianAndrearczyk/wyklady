import { PrzykladPage } from './app.po';

describe('przyklad App', function() {
  let page: PrzykladPage;

  beforeEach(() => {
    page = new PrzykladPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
