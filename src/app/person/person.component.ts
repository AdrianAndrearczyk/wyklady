import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  myForm: FormGroup;

  imie: AbstractControl;
  nazwisko: AbstractControl;

  constructor(fb: FormBuilder) {

    this.myForm = fb.group({
      'imie' : ['Ziutek', Validators.required],
      'nazwisko' : ['Zieliński', Validators.compose([Validators.required, this.malaLiteraValidator])]
    })

  this.imie = this.myForm.controls['imie'];
  this.nazwisko = this.myForm.controls['nazwisko'];

  }

  ngOnInit() {
  }
  personSubmit(form: any)
  {
    console.log("Form submitted", form);
  }
  malaLiteraValidator(control: FormControl):{[s: string]: boolean;}
  {
    if(control.hasError('required')){
      if(!control.value.match(/^[A-Z][a-z0-9_-]/))
        return {'malaLitera': true};
    }

  }
}
